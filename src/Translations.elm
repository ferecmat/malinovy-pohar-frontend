module Translations exposing (..)

type Lang
  =  En
  |  Sk

getLnFromCode: String -> Lang
getLnFromCode code =
   case code of 
      "en" -> En
      "sk" -> Sk
      _ -> En

title: Lang -> String
title lang  =
  case lang of 
      En -> "Malinovo Cup"
      Sk -> "Malinový Pohár"

navHome: Lang -> String
navHome lang  =
  case lang of 
      En -> "home"
      Sk -> "Domov"

navAbout: Lang -> String
navAbout lang  =
  case lang of 
      En -> "About run"
      Sk -> "O behu"

navRegistration: Lang -> String
navRegistration lang  =
  case lang of 
      En -> "Registration"
      Sk -> "Registrácia"

navPropositions: Lang -> String
navPropositions lang  =
  case lang of 
      En -> "Propozitions"
      Sk -> "Propozície"

navZeroWaste: Lang -> String
navZeroWaste lang  =
  case lang of 
      En -> "Zero Waste"
      Sk -> "Zero Waste"

navResults: Lang -> String
navResults lang  =
  case lang of 
      En -> "Results"
      Sk -> "Výsledky"

contacts: Lang -> String
contacts lang  =
  case lang of 
      En -> "Contact"
      Sk -> "Kontakt"

withLove: Lang -> String
withLove lang  =
  case lang of 
      En -> "Made with love by ManaBoost organization"
      Sk -> "S láskou pripravuje ManaBoost o.z."