module Main exposing (main)

import Browser
import Html
import Html.Attributes
import Html.Events as Events
import Http
import Json.Encode as Encode
import List.Extra as List
import Maybe.Extra as Maybe
import RemoteData
import Translations


type Model
    = Model
        { lang : Translations.Lang
        , activeMap : Map
        , form : Form
        }


type alias Form =
    { firstName : Maybe String
    , lastName : Maybe String
    , email : Maybe String
    , team : Maybe String
    , startNumber : Maybe Int
    , disableButton : Bool
    , status : RemoteData.RemoteData Http.Error String
    }


type Msg
    = ChangeMap Map
    | ChosenStrartNumber (Maybe Int)
    | ChosenFirstName String
    | ChosenLastName String
    | ChosenEmail String
    | ChosenTeam String
    | SubmitForm
    | SubmitedForm (Result Http.Error ())


type Map
    = Map6
    | Map11


main : Program () Model Msg
main =
    Browser.document
        { init = init
        , update = update
        , view = view
        , subscriptions = \_ -> Sub.none
        }


init : () -> ( Model, Cmd msg )
init () =
    ( Model
        { lang = Translations.Sk
        , activeMap = Map6
        , form = emptyForm
        }
    , Cmd.none
    )


emptyForm : Form
emptyForm =
    { firstName = Nothing
    , lastName = Nothing
    , email = Nothing
    , team = Nothing
    , startNumber = Nothing
    , disableButton = False
    , status = RemoteData.NotAsked
    }


update : Msg -> Model -> ( Model, Cmd Msg )
update msg (Model ({ form } as model)) =
    case msg of
        ChangeMap map ->
            ( Model { model | activeMap = map }, Cmd.none )

        ChosenStrartNumber number ->
            let
                updatedForm =
                    { form | startNumber = number }
            in
            ( Model { model | form = updatedForm }, Cmd.none )

        ChosenFirstName firstName ->
            let
                updatedForm =
                    { form | firstName = Just firstName }
            in
            ( Model { model | form = updatedForm }, Cmd.none )

        ChosenLastName lastName ->
            let
                updatedForm =
                    { form | lastName = Just lastName }
            in
            ( Model { model | form = updatedForm }, Cmd.none )

        ChosenEmail email ->
            let
                updatedForm =
                    { form | email = Just email }
            in
            ( Model { model | form = updatedForm }, Cmd.none )

        ChosenTeam team ->
            let
                updatedForm =
                    { form | team = Just team }
            in
            ( Model { model | form = updatedForm }, Cmd.none )

        SubmitForm ->
            let
                updatedForm =
                    { form | disableButton = True }
            in
            ( Model { model | form = updatedForm }, submitForm model.form )

        SubmitedForm result ->
            case result of
                Ok () ->
                    let
                        updatedForm =
                            { emptyForm | status = RemoteData.Success "Registracia sa podarila" }
                    in
                    ( Model { model | form = updatedForm }, Cmd.none )

                Err err ->
                    let
                        updatedForm =
                            { form | disableButton = True, status = RemoteData.Failure err }
                    in
                    ( Model { model | form = updatedForm }, Cmd.none )


isFilled : Form -> Bool
isFilled form =
    List.all (\x -> x)
        [ Maybe.isJust form.firstName
        , Maybe.isJust form.lastName
        , Maybe.isJust form.email
        ]


baseApiUrl : String
baseApiUrl =
    ""


submitForm : Form -> Cmd Msg
submitForm form =
    if isFilled form then
        let
            body =
                Encode.object
                    [ ( "firstName", Encode.string <| Maybe.withDefault "" <| form.firstName )
                    , ( "lastName", Encode.string <| Maybe.withDefault "" <| form.lastName )
                    , ( "email", Encode.string <| Maybe.withDefault "" <| form.email )
                    , ( "team", Encode.string <| Maybe.withDefault "" <| form.team )
                    , ( "startNumber", Encode.int <| Maybe.withDefault 0 <| form.startNumber )
                    ]
        in
        Http.post
            { url = baseApiUrl ++ "register"
            , body = Http.jsonBody body
            , expect = Http.expectWhatever SubmitedForm
            }

    else
        Cmd.none


view : Model -> Browser.Document Msg
view ((Model { lang }) as model) =
    Browser.Document (Translations.title lang)
        [ navigation lang
        , cover lang
        , mainContent model
        , contacts lang
        , Html.footer [] [ Html.text (Translations.withLove lang) ]
        ]


navigation : Translations.Lang -> Html.Html msg
navigation lang =
    Html.nav []
        [ Html.a [ Html.Attributes.id "menu-btn" ] [ Html.i [ Html.Attributes.class "fas fa-bars" ] [] ]
        , Html.ul []
            [ Html.li [] [ Html.a [ Html.Attributes.href "#domov" ] [ Html.text (Translations.navHome lang) ] ]
            , Html.li [] [ Html.a [ Html.Attributes.href "#o-behu" ] [ Html.text (Translations.navAbout lang) ] ]
            , Html.li [] [ Html.a [ Html.Attributes.href "#registracia" ] [ Html.text (Translations.navRegistration lang) ] ]
            , Html.li [] [ Html.a [ Html.Attributes.href "#zero-waste" ] [ Html.text (Translations.navZeroWaste lang) ] ]
            , Html.li [] [ Html.a [ Html.Attributes.href "#vysledky" ] [ Html.text (Translations.navResults lang) ] ]
            ]
        ]


contacts : Translations.Lang -> Html.Html msg
contacts lang =
    Html.div [ Html.Attributes.id "kontakt" ]
        [ Html.h2 [] [ Html.text (Translations.contacts lang) ]
        , Html.ul []
            [ Html.li [] [ Html.i [ Html.Attributes.class "fas fa-at" ] [], Html.text "info.malinovypohar@gmail.com" ]
            , Html.li [] [ Html.i [ Html.Attributes.class "fas fa-phone" ] [], Html.text "+421-917-915-445" ]
            , Html.li []
                [ Html.a
                    [ Html.Attributes.href "https://www.facebook.com/malinovypohar/" ]
                    [ Html.i [ Html.Attributes.class "fab fa-facebook" ] [], Html.text "facebook/malinovypohar" ]
                ]
            , Html.li []
                [ Html.a
                    [ Html.Attributes.href "https://www.instagram.com/malinovypohar/" ]
                    [ Html.i [ Html.Attributes.class "fab fa-instagram" ] [], Html.text "insta/malinovypohar" ]
                ]
            ]
        ]


cover : Translations.Lang -> Html.Html msg
cover lang =
    Html.section [ Html.Attributes.id "domov" ]
        [ Html.main_ []
            [ Html.img [ Html.Attributes.alt "MALINOVÝ POHÁR 11.9.2021", Html.Attributes.id "title", Html.Attributes.src "https://malinovypohar.sk/static/images/logo-title.svg" ]
                []
            ]
        ]


mainContent : Model -> Html.Html Msg
mainContent ((Model { lang, activeMap }) as model) =
    Html.main_ [ Html.Attributes.class "main" ]
        [ Html.section [ Html.Attributes.id "o-behu" ]
            [ Html.h1 []
                [ Html.text "O behu" ]
            , Html.p []
                [ Html.text "Malinový pohár sa uskutoční "
                , Html.b []
                    [ Html.text "11.9.2021" ]
                , Html.text ".        "
                ]
            , Html.p []
                [ Html.text "Pôvodná a už pomaly tradičná trať bude mať tentokrát až 6km (podrobne si ju môžete naštudovať z mapky) a ako            doposiaľ sa tiahne cez anglický park a ďalej malebným lužným prostredím okolo ramena Malého Dunaja. Štartuje            a končí sa priamo pred Apponyiho kaštieľom, kde sa po našej cieľovej rovinke prechádzal v roku 1910 Theodore            Roosevelt.            Dlhoočakávanou novinkou je sľubovaná nová, 11km dlhá trať, ktorou sa môžete vydať do najkrajších zákutí            Malinova!        " ]
            , Html.p []
                [ Html.text "Kategórie ženy a muži sú od tohto ročníka obohatené o novú premiérovú kategóriu Nordic walking.        " ]
            , Html.p []
                [ Html.text "Deti sa ako vždy môžu tešiť na MINIbeh okolo kaštieľa (600m), za ktorého zvládnutie ich neminie zdravá            odmena.        " ]
            , Html.p []
                [ Html.text "Pobežte s nami úplne bez odpadu! Presné propozície nájdete "
                , Html.a [ Html.Attributes.href "{{ url_for('propositions') }}" ]
                    [ Html.text "tu" ]
                , Html.text ".        "
                ]
            , Html.ul [ Html.Attributes.id "tracks" ]
                [ Html.li
                    [ Html.Attributes.classList [ ( "inactive", activeMap == Map11 ) ]
                    , Events.onClick <| ChangeMap Map6
                    ]
                    [ Html.a []
                        [ Html.text "6km trať" ]
                    ]
                , Html.li
                    [ Html.Attributes.classList [ ( "inactive", activeMap == Map6 ) ]
                    , Events.onClick <| ChangeMap Map11
                    ]
                    [ Html.a []
                        [ Html.text "11km trať" ]
                    ]
                ]
            , Html.img
                [ Html.Attributes.alt "Mapa"
                , Html.Attributes.class "map"
                , Html.Attributes.id "map-6"
                , Html.Attributes.src "https://malinovypohar.sk/static/images/map6.jpg"
                , Html.Attributes.classList [ ( "block", activeMap == Map6 ), ( "hidden", activeMap == Map11 ) ]
                ]
                []
            , Html.img
                [ Html.Attributes.alt "Mapa"
                , Html.Attributes.class "map"
                , Html.Attributes.id "map-11"
                , Html.Attributes.src "https://malinovypohar.sk/static/images/map11.jpg"
                , Html.Attributes.classList [ ( "block", activeMap == Map11 ), ( "hidden", activeMap == Map6 ) ]
                ]
                []
            ]
        , Html.section [ Html.Attributes.id "registracia" ]
            [ Html.h1 []
                [ Html.text "Registrácia" ]
            , Html.p []
                [ Html.text "Tu sa registrujte. Štartovné je 6€ (deti 4€) prevodom na účet (nezabudnite pridať variabilný symbol, ktorý            vám            vygenerujeme) alebo 8€ na mieste. Po zaplatení vám pošleme email o tom, ako veľmi sa na vás tešíme a na            štarte stačí len ak nám poviete vaše meno.        " ]
            , Html.p []
                [ Html.text "Svojou účasťou na Malinovom pohári podporujete nadáciu "
                , Html.a [ Html.Attributes.href "http://www.nadaciastastnesrdcia.sk/" ]
                    [ Html.text "Šťastné                srdcia" ]
                , Html.text ", ktorá pomáha deťom s chorými            srdiečkami.        "
                ]
            , Html.p []
                [ Html.text "Keďže jeden z organizátorov je poznačený matfyzom, namiesto štartovných čísel budete bežať so štartovnými            prvočíslami. Ak si na žiadne nespomínate, "
                , Html.a [ Html.Attributes.href "https://en.wikipedia.org/wiki/List_of_prime_numbers#The_first_1000_prime_numbers" ]
                    [ Html.text "tu" ]
                , Html.text "sa môžete            nechať inšpirovať. (alebo len nechajte prázdnu kolónku a my pre vás dáke nájdeme)        "
                ]
            , Html.div [ Html.Attributes.class "modal", Html.Attributes.id "shirt-modal" ]
                [ Html.div [ Html.Attributes.class "modal-content" ]
                    [ Html.span [ Html.Attributes.class "close" ]
                        [ Html.text "×" ]
                    , Html.section []
                        [ Html.h2 []
                            [ Html.text "Parádne tričko" ]
                        , Html.p []
                            [ Html.text "Zabudni na Gucci, zabudni na Pradu,                        s našim novým tričkom budeš najviac na parádu!" ]
                        , Html.div [ Html.Attributes.class "slideshow-container" ]
                            [ Html.div [ Html.Attributes.class "mySlides fade" ]
                                [ Html.img [ Html.Attributes.alt "", Html.Attributes.src "https://malinovypohar.sk/static/images/shirt-front.png" ]
                                    []
                                ]
                            , Html.div [ Html.Attributes.class "mySlides fade" ]
                                [ Html.img [ Html.Attributes.alt "", Html.Attributes.src "https://malinovypohar.sk/static/images/shirt-back.png" ]
                                    []
                                ]
                            , Html.a [ Html.Attributes.class "prev" ]
                                [ Html.text "❮" ]
                            , Html.a [ Html.Attributes.class "next" ]
                                [ Html.text "❯" ]
                            ]
                        ]
                    ]
                ]
            , reportRegistration model
            , registrationForm model
            ]
        , Html.section [ Html.Attributes.id "zero-waste" ]
            [ Html.h1 []
                [ Html.text "Zero Waste" ]
            , Html.p []
                [ Html.text "Chceme ukázať, že bežecké podujatie sa dá organizovať aj inak ako s hŕbou zbytočných igelitiek, pohárikov,            papierikov... proste odpadu, ktorý by nemusel vznikať, ak nastane vzájomná spolupráca nás všetkých. Sme si            vedomí, že v dnešných podmienkach je vytváranie nulového odpadu skoro nemožné, ale robíme všetko pre to, aby            sme ho vyprodukovali čo najmenej.        " ]
            , Html.p []
                [ Html.text "Už od nultého ročníka organizujeme naše preteky tak, aby mali čo najmenší dopad na miesto, v ktorom žijeme a            tejto myšlienky sa plánujeme držať aj po ďalšie ročníky. Radi privítame aj vašu pomoc či tipy a skúsenosti,            ako to spraviť ešte lepšie. Skúsme to spolu!        " ]
            , Html.h2 []
                [ Html.text "Minuloročné štatistky        " ]
            , Html.ul [ Html.Attributes.class "raspberry-list" ]
                [ Html.li []
                    [ Html.p []
                        [ Html.text "Minulý ročník sa nám podarilo na výrobu štartovných prvočísiel zrecyklovať 59 kartónových krabíc.                " ]
                    ]
                , Html.li []
                    [ Html.p []
                        [ Html.text "Na výrobu štartového oblúka sme použili pneumatiky a obkladačky z čiernej skládky a drevené koly zo                    starej terasovej konštrukcie.                " ]
                    ]
                , Html.li []
                    [ Html.p []
                        [ Html.text "Jednorazové značenie trate sme nahradili pestrofarebnými vyradenými ponožkami od našich kamarátov.                " ]
                    ]
                , Html.li []
                    [ Html.p []
                        [ Html.text "Namiesto nepotrebných igelitových balíkov dostali víťazi sklenený pohár plný malín z lokálnej                    produkcie.                " ]
                    ]
                , Html.li []
                    [ Html.p []
                        [ Html.text "Počas samotných pretekov sme aj vďaka Vám produkovali iba bio odpad pozostávajúci z ohryzkov jabĺk a                    kôstok broskýň a sliviek.                " ]
                    ]
                ]
            , Html.h2 []
                [ Html.text "Čo môžem ako bežec urobiť / radšej neurobiť?        " ]
            , Html.ul [ Html.Attributes.class "raspberry-list" ]
                [ Html.li []
                    [ Html.p []
                        [ Html.text "prinesiem si vlastný pohár na iontový nápoj (Jasné, že nie jednorazový. Ale porcelánovú čašu zo                    súpravy, čo vaša babka dostala ako svadobný dar, radšej nechajte vo vitríne.)                " ]
                    ]
                , Html.li []
                    [ Html.p []
                        [ Html.text "neprinesiem si pitie v PET fľaši                " ]
                    ]
                , Html.li []
                    [ Html.p []
                        [ Html.text "neprídem sám autom (nechám sa zviesť, niekoho ešte zveziem, skúsim bus...)                " ]
                    ]
                , Html.li []
                    [ Html.p []
                        [ Html.text "budem sa tešiť, že od organizátorov nedostanem igelitku plnú zbytočností...                " ]
                    ]
                , Html.li []
                    [ Html.p []
                        [ Html.text "čo len chcem a uznám za vhodné (Blysnite sa, inšpirujte nás, buďte frajeri!)                " ]
                    ]
                ]
            ]
        , Html.section [ Html.Attributes.id "partners" ]
            [ Html.h1 []
                [ Html.text "Partneri" ]
            , Html.p []
                [ Html.text "Chceš tu byť aj ty? "
                , Html.a [ Html.Attributes.href "#kontakt" ]
                    [ Html.text "Ozvi sa nám!" ]
                ]
            , Html.div [ Html.Attributes.id "sponsors" ]
                [ Html.div [ Html.Attributes.class "sponsor" ]
                    [ Html.a [ Html.Attributes.href "https://www.viemiconsulting.sk/" ]
                        [ Html.img [ Html.Attributes.alt "VieMi Consulting", Html.Attributes.src "https://malinovypohar.sk/static/images/sponsors/viemi.svg", Html.Attributes.title "VieMi Consulting" ]
                            []
                        ]
                    ]
                , Html.div [ Html.Attributes.class "sponsor" ]
                    [ Html.a [ Html.Attributes.href "https://www.panex.sk/" ]
                        [ Html.img [ Html.Attributes.alt "Panex", Html.Attributes.src "https://malinovypohar.sk/static/images/sponsors/panex.png", Html.Attributes.title "Panex" ]
                            []
                        ]
                    ]
                , Html.div [ Html.Attributes.class "sponsor" ]
                    [ Html.a [ Html.Attributes.href "http://www.nadaciastastnesrdcia.sk/" ]
                        [ Html.img [ Html.Attributes.alt "Nadácia Šťastné Srdcia", Html.Attributes.src "https://malinovypohar.sk/static/images/sponsors/stastne-srdcia.svg", Html.Attributes.title "Nadácia Šťastné Srdcia" ]
                            []
                        ]
                    ]
                , Html.div [ Html.Attributes.class "sponsor" ]
                    [ Html.a [ Html.Attributes.href "https://www.idcreative.sk/" ]
                        [ Html.img [ Html.Attributes.alt "id creative", Html.Attributes.src "https://malinovypohar.sk/static/images/sponsors/id-creative.jpg", Html.Attributes.title "id creative" ]
                            []
                        ]
                    ]
                , Html.div [ Html.Attributes.class "sponsor" ]
                    [ Html.a [ Html.Attributes.href "https://www.ambulanciamalina.sk/" ]
                        [ Html.img [ Html.Attributes.alt "Detská ambulancia Malina", Html.Attributes.src "https://malinovypohar.sk/static/images/sponsors/malina.png", Html.Attributes.title "Detská ambulancia Malina" ]
                            []
                        ]
                    ]
                , Html.div [ Html.Attributes.class "sponsor" ]
                    [ Html.a [ Html.Attributes.href "https://www.wiky.sk/" ]
                        [ Html.img [ Html.Attributes.alt "Wiki", Html.Attributes.src "https://malinovypohar.sk/static/images/sponsors/wiki.svg", Html.Attributes.title "Wiki" ]
                            []
                        ]
                    ]
                , Html.div [ Html.Attributes.class "sponsor" ]
                    [ Html.a [ Html.Attributes.href "https://bratislavskykraj.sk/" ]
                        [ Html.img [ Html.Attributes.alt "Bratislavský samosprávny kraj", Html.Attributes.src "https://malinovypohar.sk/static/images/sponsors/bsk.svg", Html.Attributes.title "Bratislavský samosprávny kraj" ]
                            []
                        ]
                    ]
                , Html.div [ Html.Attributes.class "sponsor" ]
                    [ Html.a [ Html.Attributes.href "https://www.zse.sk/" ]
                        [ Html.img [ Html.Attributes.alt "ZSE", Html.Attributes.src "https://malinovypohar.sk/static/images/sponsors/zse.svg", Html.Attributes.title "ZSE" ]
                            []
                        ]
                    ]
                , Html.div [ Html.Attributes.class "sponsor" ]
                    [ Html.a [ Html.Attributes.href "http://www.ovocnesnejky.sk/" ]
                        [ Html.img [ Html.Attributes.alt "Ovocné SNEJKY", Html.Attributes.src "https://malinovypohar.sk/static/images/sponsors/ovocnesnejky.png", Html.Attributes.title "Ovocné SNEJKY" ]
                            []
                        ]
                    ]
                , Html.div [ Html.Attributes.class "sponsor" ]
                    [ Html.a [ Html.Attributes.href "https://www.muzeumpezinok.sk/sk" ]
                        [ Html.img [ Html.Attributes.alt "Malokarpatské múzeum v Pezinku", Html.Attributes.src "https://malinovypohar.sk/static/images/sponsors/mmvp.jpg", Html.Attributes.title "Malokarpatské múzeum v Pezinku" ]
                            []
                        ]
                    ]
                , Html.div [ Html.Attributes.class "sponsor" ]
                    [ Html.a [ Html.Attributes.href "https://www.toitoi.sk/" ]
                        [ Html.img [ Html.Attributes.alt "TOI TOI & DIXI", Html.Attributes.src "https://malinovypohar.sk/static/images/sponsors/toitoi.png", Html.Attributes.title "TOI TOI & DIXI" ]
                            []
                        ]
                    ]
                , Html.div [ Html.Attributes.class "sponsor" ]
                    [ Html.a [ Html.Attributes.href "https://www.dobryjezko.sk/" ]
                        [ Html.img [ Html.Attributes.alt "DOBRÝ JEŽKO", Html.Attributes.src "https://malinovypohar.sk/static/images/sponsors/dobryjezko.svg", Html.Attributes.title "DOBRÝ JEŽKO" ]
                            []
                        ]
                    ]
                , Html.div [ Html.Attributes.class "sponsor" ]
                    [ Html.a [ Html.Attributes.href "https://www.funmagazin.sk/rytmus-a-jasmina-sa-stahuju-takto-vyzera-ich-honosne-sidlo-za-580-000-eur-vo-vnutri/" ]
                        [ Html.img [ Html.Attributes.alt "Obec Malinovo", Html.Attributes.src "https://malinovypohar.sk/static/images/sponsors/malinovo.png", Html.Attributes.title "Obec Malinovo" ]
                            []
                        ]
                    ]
                ]
            ]
        , Html.section [ Html.Attributes.id "vysledky" ]
            [ Html.h1 []
                [ Html.text "Výsledky" ]
            , Html.div [ Html.Attributes.id "results" ]
                [ Html.a [ Html.Attributes.href "{{ url_for('vysledky2021') }}" ]
                    [ Html.text "Tohoročné výsledky" ]
                , Html.a [ Html.Attributes.href "{{ url_for('vysledky') }}" ]
                    [ Html.text "Výsledky minulých ročnikov" ]
                ]
            , Html.h2 []
                [ Html.text "Rekordy Trate" ]
            , Html.div [ Html.Attributes.id "records" ]
                [ Html.div [ Html.Attributes.class "record" ]
                    [ Html.h3 []
                        [ Html.text "Kategória Ženy" ]
                    , Html.b []
                        [ Html.text "Jeanette Borhy" ]
                    , Html.text "s číslom 383 v roku 2020 a s doposiaľ neprekonaným časom                "
                    , Html.b []
                        [ Html.text "00:21:49.00" ]
                    , Html.text "!            "
                    ]
                , Html.div []
                    [ Html.h3 []
                        [ Html.text "Kategória Muži" ]
                    , Html.b []
                        [ Html.text "Patrik Lopušný" ]
                    , Html.text "s číslom 337 v roku 2019 a s doposiaľ neprekonaným časom "
                    , Html.b []
                        [ Html.text "00:19:57.73" ]
                    , Html.text "!            "
                    ]
                ]
            ]
        , Html.section [ Html.Attributes.id "o-nas" ]
            [ Html.h1 []
                [ Html.text "O nás" ]
            , Html.div []
                [ Html.h2 []
                    [ Html.text "Heňo" ]
                , Html.img [ Html.Attributes.alt "Heňo", Html.Attributes.src "https://malinovypohar.sk/static/images/heno.jpg" ]
                    []
                , Html.p []
                    [ Html.text "Behajúci baleťák. Ak nie je práve zatvorený na baletnej sále na VŠMU, môžete ho zazrieť na divadelných                doskách alebo priamo za domom, v divokých Malinovských chotároch. Práve tam sa raz zrodila myšlienka                združiť všetkých behuchtivých do jednej veľkej malinovskej rodiny a spolu si zmerať sily!            " ]
                ]
            , Html.div []
                [ Html.h2 []
                    [ Html.text "Maťko" ]
                , Html.img [ Html.Attributes.alt "Maťko", Html.Attributes.src "https://malinovypohar.sk/static/images/matko.jpg" ]
                    []
                , Html.p []
                    [ Html.text "Maratónsky matfyzák. Ak sa práve nepokúša testovať možnosti svojho tela na ultrapodujatiach alebo                prechodoch škótskou či balkánskou divočinou, tak rád derivuje kopce ako konštanty. Rukami behá po                klávesnici a programuje našu stránku, nohami behá po Malinove a organizuje.            " ]
                ]
            ]
        ]


reportRegistration : Model -> Html.Html msg
reportRegistration ((Model { lang, form }) as model) =
    case form.status of
        RemoteData.Success msg ->
            Html.text msg

        RemoteData.Failure _ ->
            Html.text "Unable to register"

        _ ->
            Html.text ""


registrationForm : Model -> Html.Html Msg
registrationForm ((Model { lang, form }) as model) =
    Html.form
        [ Events.onSubmit SubmitForm ]
        [ Html.input
            [ Html.Attributes.id "first_name"
            , Html.Attributes.name "first_name"
            , Html.Attributes.placeholder "Meno"
            , Html.Attributes.attribute "required" ""
            , Html.Attributes.type_ "text"
            , Html.Attributes.value (Maybe.withDefault "" form.firstName)
            , Events.onInput ChosenFirstName
            ]
            []
        , Html.input
            [ Html.Attributes.id "last_name"
            , Html.Attributes.name "last_name"
            , Html.Attributes.placeholder "Priezvisko"
            , Html.Attributes.attribute "required" ""
            , Html.Attributes.type_ "text"
            , Html.Attributes.value (Maybe.withDefault "" form.lastName)
            , Events.onInput ChosenLastName
            ]
            []
        , Html.input
            [ Html.Attributes.id "email"
            , Html.Attributes.name "email"
            , Html.Attributes.placeholder "Email"
            , Html.Attributes.attribute "required" ""
            , Html.Attributes.type_ "text"
            , Html.Attributes.value (Maybe.withDefault "" form.email)
            , Events.onInput ChosenEmail
            ]
            []
        , Html.input
            [ Html.Attributes.id "team"
            , Html.Attributes.name "team"
            , Html.Attributes.placeholder "Tím"
            , Html.Attributes.type_ "text"
            , Html.Attributes.value (Maybe.withDefault "" form.team)
            , Events.onInput ChosenTeam
            ]
            []
        , Html.input
            [ Html.Attributes.id "start_number"
            , Html.Attributes.name "start_number"
            , Html.Attributes.placeholder "Štartovné prvočíslo"
            , Html.Attributes.type_ "text"
            , Html.Attributes.value
                (if form.startNumber == Nothing then
                    ""

                 else
                    String.fromInt <| Maybe.withDefault 0 form.startNumber
                )
            , Events.onInput (ChosenStrartNumber << String.toInt)
            ]
            []
        , Html.input
            [ Html.Attributes.id "submit"
            , Html.Attributes.name "submit"
            , Html.Attributes.type_ "submit"
            , Html.Attributes.value "Registruj ma!"
            , Html.Attributes.disabled <| form.disableButton || (not <| isFilled form)
            ]
            []
        ]
